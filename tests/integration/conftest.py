import pop.hub
import pytest


@pytest.fixture()
def hub():
    h = pop.hub.Hub()
    h.pop.sub.add(dyne_name="loop")
    yield h
