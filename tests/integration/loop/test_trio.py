import asyncio


def test_backend(hub):
    hub.loop.init.create("trio")
    assert "TrioEventLoop" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() in ("trio", "unknown")
    assert hub.loop.init.BACKEND == "trio"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
