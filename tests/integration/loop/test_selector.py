import asyncio


def test_backend(hub):
    hub.loop.init.create("selector")
    assert "Selector" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() in ("asyncio", "unknown")
    assert hub.loop.init.BACKEND == "selector"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
