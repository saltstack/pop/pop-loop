import asyncio

import pytest

try:
    import PyQt5.QtWidgets as pyqt5

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


def setup_module():
    if not HAS_LIBS:
        pytest.skip("These tests only run with QT pre-installed")


def test_backend(hub):
    hub.loop.qt.APP = pyqt5.QApplication([])
    hub.loop.init.create("qt")
    assert "Q" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "qt"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
