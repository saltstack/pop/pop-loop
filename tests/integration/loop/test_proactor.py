import asyncio
import os

import pytest


def setup_module():
    if not os.name == "nt":
        pytest.skip("These tests only run on windows")


def test_backend(hub):
    hub.loop.init.create("proactor")
    assert isinstance(hub.pop.loop.CURRENT_LOOP, asyncio.ProactorEventLoop)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "proactor"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
